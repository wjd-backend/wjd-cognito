<?php

namespace wjd\WjdCognito;

class WjdCognitoClient
{  
    /**
     * @var array
     */
    protected $config;

    protected $client;

    /**
     * WjdCognitoClient constructor
     * 
     * @param $config
     */
    public function __construct($config) 
    {
        $this->config = $config;
        $aws = new \Aws\Sdk($config);
        $cognitoClient = $aws->createCognitoIdentityProvider();
        
        $this->client = new \pmill\AwsCognito\CognitoClient($cognitoClient);
        $this->client->setAppClientId($this->config['app_client_id']);
        $this->client->setAppClientSecret($this->config['app_client_secret']);
        $this->client->setRegion($this->config['region']);
        $this->client->setUserPoolId($this->config['user_pool_id']);
    }

    public function isWjdUserLoggedIn() 
    {
        // check for lastAuthUser cookie, if not existing there can't be an access token
        if (!isset($_COOKIE['CognitoIdentityServiceProvider_'.$this->config['app_client_id'].'_LastAuthUser'])) 
            return null;

        // check for content of LastAuthUser
        $username = $_COOKIE['CognitoIdentityServiceProvider_'.$this->config['app_client_id'].'_LastAuthUser'];
        if (!isset($username)) 
            return null;
  
        $cookieUsername = urlencode(str_replace('.', '_', $username));
        $accessToken = $_COOKIE['CognitoIdentityServiceProvider_'.$this->config['app_client_id'].'_'.$cookieUsername.'_accessToken'];
        try
        {
            $user = $this->client->getUserByToken($accessToken);
            return $user;
        }
        catch (Exception $e) 
        {
            return null;
        }
    }
}
